import 'package:dart_application_calculator/dart_application_calculator.dart'
    as dart_application_calculator;
import 'dart:io';

void main() {
  print('Please input number a: ');
  int a = int.parse(stdin.readLineSync()!);
  print('Please input number b: ');
  int b = int.parse(stdin.readLineSync()!);

  int sum = Calculator.addition(a, b);
  print('Result of addition is : $sum');

  int subtract = Calculator.subtraction(a, b);
  print('Result of subtraction is : $subtract');

  int multi = Calculator.multiplecation(a, b);
  print('Result of multiple is : $multi');

  double divide = Calculator.divistion(a, b);
  print('Result of division is : $divide');

  int mod = Calculator.modulus(a, b);
  print('Result of modulus is : $mod');

  int expo = Calculator.exponent(a, b);
  print('Result of exponent is : $expo');
}

class Calculator {
  static int addition(int a, int b) {
    return a + b;
  }

  static int subtraction(int a, int b) {
    return a - b;
  }

  static int multiplecation(int a, int b) {
    return a * b;
  }

  static double divistion(int a, int b) {
    return a / b;
  }

  static int modulus(int a, int b) {
    return a % b;
  }

  static int exponent(int a, int b) {
    int expo = a;
    int result = 1;
    for (int i = 0; i < b; i++) {
      result = result * expo;
    }
    return result;
  }
}
